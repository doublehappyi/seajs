/**
 * Created by db on 14-12-10.
 */

(function($, _, Backbone){
    var User = window.User =window.User||{};
    User.aCollection = Backbone.Collection.extend({
        model:User.aModel,
        sync:function(collection,method,ops){
            var me = this;
            $.ajax({
                url:"./user.json",
                success:function(data){
                    console.log(data);
                    me.reset(data);
                }
            });
        }

    });

})(Zepto, _, Backbone);