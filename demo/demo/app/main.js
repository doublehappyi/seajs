/**
 * Created by db on 14-12-10.
 */
/*router*/
var AppRouter = Backbone.Router.extend({
    routes: {
        '': 'main',
        'a': 'renderList',
        'b': 'renderDetail',
        'c': 'renderError'
    },
    main: function () {
        console.log('应用入口方法');
        new User.aView;
    },
    renderList: function () {
        console.log('渲染列表方法');
    },
    renderDetail: function (id) {
        console.log('渲染详情方法, id为: ' + id);
    },
    renderError: function (error) {
        console.log('URL错误, 错误信息: ' + error);
    }
});

var router = new AppRouter();
Backbone.history.start();