/**
 * Created by db on 14-12-10.
 */
(function ($, _, Backbone) {
    var User = window.User = window.User || {}, htmls = User.htmls;
    User.aView = Backbone.View.extend({
        tagName:"div",
        className:"user",

        collection:new User.aCollection,
        model:new Backbone.Model,
        initialize: function () {
            this.initHtml();
            this.initEvent();
            this.model.set({username:"ypf",password:222});
            this.listenTo(this.collection,"reset",this.render);
            this.listenTo(this.model,"change",this.renderuser);

        },
        initHtml: function () {
            this.$el.html(_.template(htmls.LoginHtml)).appendTo('body');
        },
        initEvent:function(){
            var me  = this;
            $("h2").click(function(){
                me.collection.fetch();
            });
            $("div").click(function(){
                me.model.set({age:27});
                console.log("XX"+me.model.toJSON())
            });
        },
        render:function(){
            var me = this;
            me.collection.each(function(model,index){
                var userHtml = "<div>"+model.get('username')+"</div><div>"+model.get('password')+"</div>";
                $(userHtml).appendTo("h2");

        });
        },
        renderuser:function(){
            alert("数据改变了");
        }
    });
})(Zepto, _, Backbone);