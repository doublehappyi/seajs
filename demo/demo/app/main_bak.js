/**
 * Created by rt.yishuangxi on 2014/12/10.
 */
define(function (require, exports, module) {
//    require("zepto");
//    require("underscore");
//    require("backbone");
//    require("./Models/aModel");
//    require("./Collections/aCollection");
//    require("./Views/aView");

    /*model*/
    var Person = Backbone.Model.extend({
        initialize: function () {
            console.log("hello backbone zz...");
        }
    });

    var p = new Person;

    /*router*/
    var AppRouter = Backbone.Router.extend({
        routes: {
            '': 'main',
            'a': 'renderList',
            'b': 'renderDetail',
            'c': 'renderError'
        },
        main: function () {
            console.log('应用入口方法');
        },
        renderList: function () {
            console.log('渲染列表方法');
        },
        renderDetail: function (id) {
            console.log('渲染详情方法, id为: ' + id);
        },
        renderError: function (error) {
            console.log('URL错误, 错误信息: ' + error);
        }
    });

    var router = new AppRouter();
    Backbone.history.start();
});