/**
 * Created by rt.yishuangxi on 2014/12/12.
 */
define(function (require, exports, module) {
    require("backbone");
    var cCheck = require("collections/cCheck");
    var mCheck = require("models/mCheck");
    var tCheck = require("tpls/tCheck");
    //分店数据模型
    module.exports = Backbone.View.extend({
        page: "./app/check.html",
        collection: new cCheck,
        template: _.template(tCheck),
        initialize: function () {
            console.log("views/check initialize ...");
            var self = this;
            Backbone.ajax({
                url: self.page,
                success: function (data) {
                    $(".check").append(data);
                    self.listenTo(self.collection, "reset", self.render);
                    self.collection.fetch();
                    self.bindEvents();
                }
            });
        },
        render: function () {
            var self = this;
            self.collection.each(function (model, i) {
                $(self.template(model.toJSON())).appendTo(".check");
            });
        },
        renderChange: function () {
            alert("model changed");
        },
        bindEvents:function(){
            $(".j-check").click(function(){
                console.log("j-check click...");
            });
        }
    });
});