/**
 * Created by rt.yishuangxi on 2014/12/12.
 */
define(function (require, exports, module) {
    require("backbone");
    module.exports = Backbone.Router.extend({
        routes: {
            "": "renderIndex",
            "check": "renderCheck"
        },
        renderIndex: function () {
            console.log("renderIndex in routers/main...");
        },
        renderCheck: function () {
            var vCheck = require("views/vCheck");
            new vCheck;

//            console.log("renderCheck...");
//            var mCheck = require("models/mCheck");
//            console.log("renderCheck in routers/main...");
//            var mCheck = require("models/mCheck");
//            var mBranch = new mCheck.mBranch;
//
//            console.log("branch: ",mBranch.toJSON());
//
//            var vCheck = require("views/vCheck");
//            var vBranch = new vCheck.vBranch;
//
//            var script = document.createElement("script");
//            script.setAttribute("src","./modules/core/require.js");
//            script.setAttribute("type","text/javascript");
//            document.body.appendChild(script);
        }
    });
});