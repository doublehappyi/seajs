/**
 * Created by db on 14-12-14.
 */
define(function(require, exports, module){
    require("backbone");
    var mCheck = require("models/mCheck");
    module.exports = Backbone.Collection.extend({
        model:mCheck,
        initialize:function(){
            console.log("collections.cCheck init...");
        },
        sync:function(){
            var self = this;
            Backbone.ajax({
                url:"./data/mCheck.json",
                success:function(data){
                    self.reset(data);
                }
            });
        }
    });
});